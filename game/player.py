class Player:
    def __init__(self, player_type, map):
        self._map = map
        self._player_type = player_type

    def make_move(self, x, y):
        return self._map.set_field(x, y, self._player_type)

    def get_player_type(self):
        return self._player_type
