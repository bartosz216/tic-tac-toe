from game.player import Player
from game.map import Field
import pygame
from copy import deepcopy
from operator import itemgetter
from enum import Enum

class State:
    def __init__(self, map):
        self.field_size = map.fields_number
        self.map = [[ map.get_field(i, j) for j in range(self.field_size)] for i in range(self.field_size)]
        self.available_fields = set([ (i, j) for j in range(self.field_size) for i in range(self.field_size)  if map.is_empty_field(i, j)])

    def set_field(self, x, y, field):
        self.map[x][y] = field
        self.available_fields.remove((x, y))
    
    def clear_field(self, x, y):
        self.map[x][y] = Field.empty
        self.available_fields.add((x, y))

class AlfaBetaPlayer(Player):
    class GameState(Enum):
        win = 0,
        draw = 1,
        loss = 2

    def __init__(self, map_size, fields_num, player_type, map):
        super().__init__(player_type, map)
        self._field_size = map_size/fields_num
        self.fields_number = fields_num

    def on_event(self):
        (x, y) = self._get_best_move()
        return self.make_move(x, y)

    def on_loop(self):
        pass

    def _get_best_move(self):
        state = State(self._map)
        (score, x, y) = self._alfa_beta(self._player_type, state, -50000, 50000)
        return x, y

    def _alfa_beta(self, player_type, map, alfa, beta):
        if len(map.available_fields) == 0:
            return self._state_score(map), 0, 0

        results = []
        for field in map.available_fields:
            map.set_field(field[0], field[1], player_type)
            state = self._get_state(map)
            if state == AlfaBetaPlayer.GameState.win:
                map.clear_field(field[0], field[1])
                return 10000 + len(map.available_fields)*10, field[0], field[1]
            elif state == AlfaBetaPlayer.GameState.loss:
                map.clear_field(field[0], field[1])
                return 0, field[0], field[1]
            (score, _, _) = self._alfa_beta(self._change_type(player_type), map, alfa, beta)
            map.clear_field(field[0], field[1])
            results.append((score, field[0], field[1]))
            if player_type == self._player_type:
                alfa = max(score, alfa)
                if alfa >= beta:
                    break
            else:
                beta = min(score, beta)
                if alfa >= beta:
                    break

        if player_type == self._player_type:
            return max(results, key=itemgetter(0))
        else:
            return min(results, key=itemgetter(0))

    def _change_type(self, player_type):
        if player_type == Field.circle:
            return Field.cross
        else:
            return Field.circle

    def _get_state(self, map):
        my_state = self._check_winner(map, self._player_type)
        opponent_state = self._check_winner(map, self._change_type(self._player_type))
        if my_state == True:
            return AlfaBetaPlayer.GameState.win
        elif opponent_state == True:
            return AlfaBetaPlayer.GameState.loss
        else:
            return AlfaBetaPlayer.GameState.draw

    def _state_score(self, map):
        result = self._get_state(map)
        score = {
            AlfaBetaPlayer.GameState.win: 10000 + len(map.available_fields)*10,
            AlfaBetaPlayer.GameState.loss: 0,
            AlfaBetaPlayer.GameState.draw: 500,
        }

        return score.get(result)
 
    def _check_winner(self, map, player_type):
        for i in range(self.fields_number):
            won = True
            for j in range(self.fields_number):
                if map.map[i][j] != player_type:
                    won = False
                    break
            if won == True:
                return True

        for i in range(self.fields_number):
            won = True
            for j in range(self.fields_number):
                if map.map[j][i] != player_type:
                    won = False
                    break
            if won == True:
                return True

        won = True
        for i in range(self.fields_number):
            if map.map[i][i] != player_type:
                won = False
                break
        if won == True:
            return True

        won = True
        for i in range(self.fields_number):
            if map.map[i][self.fields_number - 1 - i] != player_type:
                won = False
                break
        if won == True:
            return True
        return False
