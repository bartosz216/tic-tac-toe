from game.player import Player
import pygame

class HumanPlayer(Player):
    def __init__(self, map_size, fields_num, player_type, map):
        super().__init__(player_type, map)
        self._field_size = map_size/fields_num

    def on_event(self):
        for event in pygame.event.get(pygame.MOUSEBUTTONDOWN):
            if event.button == 1:
                x = int(event.pos[1]/self._field_size)
                y = int(event.pos[0]/self._field_size)
                return self.make_move(x, y)
        return False

    def on_loop(self):
        pass