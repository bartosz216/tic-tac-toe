import pygame
import random
from game.map import Map, Field
from game.human_player import HumanPlayer
from game.alfabeta_player import AlfaBetaPlayer

class App:
    def __init__(self):
        self._running = True
        self._is_winner = False
        self._winning_line = None
        self._display_surf = None
        self.size = self.weight, self.height = 600, 600
        self.fields_number = 3
        self.map = Map(self.weight, self.fields_number)
        self.players = [HumanPlayer(self.weight, self.fields_number, Field.circle, self.map),
                        AlfaBetaPlayer(self.weight, self.fields_number, Field.cross, self.map)]
        self.current_player = random.randint(0, len(self.players) - 1)

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size)
        pygame.display.set_caption('Tic tac toe')
        self._running = True

    def on_event(self):
        for event in pygame.event.get(pygame.QUIT):
            self._running = False
        if self._is_winner == False and self.players[self.current_player].on_event() == True:
            self.check_winner()
            if self._is_winner == False:
                if self.map.get_available_fields_number() != 0:
                    self.current_player = (self.current_player + 1) % len(self.players)
                else:
                    self._is_winner = True
                    print("Draw.")
            else:
                print("Player " + str(self.players[self.current_player].get_player_type()) + " won.")
        pygame.event.clear()

    def on_render(self):
        self._display_surf.fill((255, 255, 255))
        self.map.on_render(self._display_surf)
        if self._winning_line != None:
            start_pos = self._winning_line[0]
            end_pos = self._winning_line[1]
            pygame.draw.line(self._display_surf, (255,0,0), start_pos, end_pos, 2)
        pygame.display.update()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False
        while(self._running):
            self.on_event()
            self.on_render()
        self.on_cleanup()

    def check_winner(self):
        player_type = self.players[self.current_player].get_player_type()
        offset = 600/self.fields_number
        for i in range(self.fields_number):
            won = True
            for j in range(self.fields_number):
                if self.map.get_field(i, j) != player_type:
                    won = False
                    break
            if won == True:
                self._is_winner = True
                self._winning_line = (0, i*offset + offset/2), (self.weight, i*offset + offset/2)
                return

        for i in range(self.fields_number):
            won = True
            for j in range(self.fields_number):
                if self.map.get_field(j, i) != player_type:
                    won = False
                    break
            if won == True:
                self._is_winner = True
                self._winning_line = (i*offset + offset/2, 0), (i*offset + offset/2, self.weight)
                return 

        won = True
        for i in range(self.fields_number):
            if self.map.get_field(i, i) != player_type:
                won = False
                break
        if won == True:
            self._is_winner = True
            self._winning_line = (0, 0), (self.weight, self.weight)
            return

        won = True
        for i in range(self.fields_number):
            if self.map.get_field(i, self.fields_number - 1 - i) != player_type:
                won = False
                break
        if won == True:
            self._is_winner = True
            self._winning_line = (0, self.weight), (self.weight, 0)
