import pygame
from enum import Enum

class Field(Enum):
    empty = 0
    cross = 1
    circle = 2

class Map:
    def __init__(self, map_size, fields_number = 3):
        self.fields_number = fields_number
        self.map_size = map_size
        self.field_size = map_size/fields_number
        self._map = [[ Field.empty for _ in range(fields_number)] for _ in range(fields_number)]
        self._available_fields = set([ (i, j) for j in range(fields_number) for i in range(fields_number)])
        self._color = (0,0,0)
        self._offset = self.field_size/4

    def on_render(self, display_surf):
        for i in range(1, self.fields_number):
            pygame.draw.line(display_surf, self._color, (0, i*self.field_size), (self.map_size, i*self.field_size), 2)
            pygame.draw.line(display_surf, self._color, (i*self.field_size, 0), (i*self.field_size, self.map_size), 2)
        for i in range(self.fields_number):
            for j in range(self.fields_number):
                if self._map[i][j] == Field.circle:
                    self._draw_circle(i, j, display_surf)
                elif self._map[i][j] == Field.cross:
                    self._draw_x(i, j, display_surf)
    
    def is_empty_field(self, x, y):
        return self._map[x][y] == Field.empty

    def set_field(self, x, y, field):
        if self.is_empty_field(x, y):
            self._map[x][y] = field
            self._available_fields.remove((x, y))
            return True
        return False

    def get_field(self, x, y):
        return self._map[x][y]

    def get_available_fields_number(self):
        return len(self._available_fields)

    def get_available_fields(self):
        return self._available_fields

    def _draw_x(self, i, j, display_surf):
        j_pos = j*self.field_size + self._offset
        j_cross_pos = (j+1)*self.field_size - self._offset
        i_pos = i*self.field_size + self._offset
        i_cross_pos = (i+1)*self.field_size - self._offset

        start_pos = (j_pos, i_pos)
        end_pos = (j_cross_pos, i_cross_pos)
        pygame.draw.line(display_surf, self._color, start_pos, end_pos, 2)

        start_pos = (j_cross_pos, i_pos)
        end_pos = (j_pos, i_cross_pos)
        pygame.draw.line(display_surf, self._color, start_pos, end_pos, 2)

    def _draw_circle(self, i, j, display_surf):
        center_x = int(j*self.field_size + self.field_size/2)
        center_y = int(i*self.field_size + self.field_size/2)
        center = (center_x, center_y)
        radius = int(self._offset)
        pygame.draw.circle(display_surf, self._color, center, radius, 1)
